# Group 6 FIN556 FA23 Final Report

## Teammates

**Fang Yi, Lin**
- I am FY Lin, an Computer Science undergraduate at the Univerisity of Illinois at Urbana-Champaign. I have taken courses related to ML and am interesting in ML/deep learning, specifically NLP. My skills include C++, Python, Database Systems.
- Linkedin: www.linkedin.com/in/fang-yi-lin-762b31197/
- Email: fyl2@illinois.edu 

**Adarsh Dayalan**
- I am Adarsh Dayalan, a Computer Science undergraduate at the Univerisity of Illinois at Urbana-Champaign. I have taken courses related to AI, ML, and algorithms design. My skills include languages such as Python, C++, Java. I also am experienced with full stack dev, and ML research.
- Linkedin: https://www.linkedin.com/in/adarsh-dayalan/
- Email: dayalan2@illinois.edu